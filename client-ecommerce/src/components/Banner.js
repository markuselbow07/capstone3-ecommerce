import { Button, Row, Col } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

export default function Banner ({data}){
	console.log(data)
	const {title, content, destination, label} = data;
	
		return (
			<Row>
				<Col className="p-4">
					<h1>{title}</h1>
					<p>{content}</p>
					{/*<Link to={destination}>{label}</Link>*/}
					<Button variant="outline-dark" size="lg" as={NavLink} to={destination}>{label}</Button>
				</Col>
			</Row>
		)		
}

