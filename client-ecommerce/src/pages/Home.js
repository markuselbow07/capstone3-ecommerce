import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){
	const data={
		title: "Ramen House",
		content: "Best tasting ramen in Hometown",
		destination: "/products",
		label: "Order Now!"
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}
